var particles = require('./particles');
var nav = require('./nav');
var resultsPage = require('./results-page');

document.addEventListener('DOMContentLoaded', function() {
	if (document.querySelector('.lp__container'))
		particles.init(document.querySelector('.lp__container'), {});

	if (document.getElementById('nav'))
		nav.init(document.getElementById('nav'));

	if (document.getElementById('results-page'))
		resultsPage.init();
});