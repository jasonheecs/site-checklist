/*jshint node: true */
/*globals document*/
/*globals window*/
"use strict";

// http://youmightnotneedjquery.com/#deep_extend
function extend(out) {
	out = out || {};

	for (var i = 1; i < arguments.length; i++) {
		var obj = arguments[i];

		if (!obj)
		  continue;

		for (var key in obj) {
		  if (obj.hasOwnProperty(key)) {
		    if (typeof obj[key] === 'object')
		      out[key] = extend(out[key], obj[key]);
		    else
		      out[key] = obj[key];
		  }
		}
	}

	return out;
}

/**
 * function to throttle js calls
 */
function throttle(callback, limit) {
	var wait = false;
	return function() {
		if (!wait) {
			callback.call();
			wait = true;
			setTimeout(function() {
				wait = false;
			}, limit);
		}
	};
}

var element;
var canvasSupport = !!document.createElement('canvas').getContext;
var canvas;
var ctx;
var particles = [];
var raf;
var options; //specified options via init
//default options
var defaults = {
	minSpeedX: 0.1,
	maxSpeedX: 0.7,
	minSpeedY: 0.1,
	maxSpeedY: 0.7,
	density: 10000, // How many particles will be generated: one particle every n pixels
	dotColor: 'rgba(102,102,102,0.35)',
	lineColor: 'rgba(102,102,102,0.35)',
	particleRadius: 7, //dot size
	lineWidth: 1,
	proximity: 100 // How close two dots need to be before they join
};

function init(_element, _options) {
	options = extend({}, defaults, _options);
	element = _element;

	if (!canvasSupport)
		return;

	//create canvas
	canvas = document.createElement('canvas');
	canvas.className = 'particles-canvas';
	canvas.style.display = 'block';
	element.insertBefore(canvas, element.firstChild);
	ctx = canvas.getContext('2d');
	styleCanvas(element, canvas);

	//create particles
	var numParticles = Math.round((canvas.width * canvas.height) / options.density);
	for (var i = 0; i < numParticles; i++) {
		var p = new Particle();
		p.setStackPos(i);
		particles.push(p);
	}

	window.addEventListener('resize', throttle(resizeHandler, 150));

	draw();
}

/**
 * Style the canvas
 */
function styleCanvas(element, canvas) {
	canvas.width = element.offsetWidth;
	canvas.height = element.offsetHeight;
	ctx.fillStyle = options.dotColor;
	ctx.strokeStyle = options.lineColor;
	ctx.lineWidth = options.lineWidth;
}

/**
 * Add/remove particles on window resize
 */
function resizeHandler() {
	//resize the canvas
	styleCanvas(element, canvas);

	var elWidth = element.offsetWidth;
	var elHeight = element.offsetHeight;

	//remove particles outside the canvas
	for (var i = particles.length - 1; i >=0; i--) {
		if (particles[i].position.x > elWidth || particles[i].position.y > elHeight)
			particles.splice(i, 1);
	}

	//adjust particles density
	var numParticles = Math.round((canvas.width * canvas.height) / options.density);
	if (numParticles > particles.length) {
		while (numParticles > particles.length) {
			var p = new Particle();
			particles.push(p);
		}
	} else if (numParticles < particles.length) {
		particles.splice(numParticles);
	}

	//reindex particles
	particles.forEach(function(particle, i) {
		particle.setStackPos(i);
	});
}

function draw() {
	//wipe canvas
	ctx.clearRect(0, 0 , canvas.width, canvas.height);

	//update particle positions and draw
	particles.forEach(function(particle) {
		particle.updatePosition();
		particle.draw();
	});

	window.requestAnimationFrame(draw);
}

/**
 * Particle object
 */
function Particle() {
	this.stackPos = 0;
	this.active = true;
	this.layer = Math.ceil(Math.random() * 3);
	//initial particle position
	this.position = {
		x: Math.ceil(Math.random() * canvas.width),
		y: Math.ceil(Math.random() * canvas.height)
	};

	//Random particle speed, within min and max values
	this.speed = {};
	this.speed.x = +((-options.maxSpeedX / 2) + (Math.random() * options.maxSpeedX)).toFixed(2);
	this.speed.x += this.speed.x > 0 ? options.minSpeedX : -options.minSpeedX;
	this.speed.y = +((-options.maxSpeedY / 2) + (Math.random() * options.maxSpeedY)).toFixed(2);
	this.speed.y += this.speed.y > 0 ? options.minSpeedY: -options.minSpeedY;
}

Particle.prototype.draw = function() {
	//draw circle
	ctx.beginPath();
	ctx.arc(this.position.x, this.position.y, options.particleRadius / 2, 0, Math.PI * 2, true);
	ctx.closePath();
	ctx.fill();

	//draw lines
	ctx.beginPath();
	//iterate over all particles which are higher in the stack than this one
	for (var i = particles.length - 1; i > this.stackPos; i--) {
		var p2 = particles[i];

		//Pythagorus theorum to get distance between 2 pts
		var a = this.position.x - p2.position.x;
		var b = this.position.y - p2.position.y;
		var dist = Math.sqrt((a * a) + (b * b)).toFixed(2);

		//if two particles are within proximity, join them
		if (dist < options.proximity) {
			ctx.moveTo(this.position.x, this.position.y);
			ctx.lineTo(p2.position.x, p2.position.y);
		}
	}
	
	ctx.stroke();
	ctx.closePath();
};

Particle.prototype.updatePosition = function() {
	var elWidth = element.offsetWidth;
	var elHeight = element.offsetHeight;

	//if particle has reached edge of canvas
	if (this.position.y + this.speed.y > elHeight || this.position.y + this.speed.y < 0)
		this.speed.y = -this.speed.y;

	if (this.position.x > elWidth) {
		this.position.x = 0;
	}
	else if (this.position.x < 0) {
		this.position.x = elWidth;
	}

	//move particle
	this.position.x += this.speed.x;
	this.position.y += this.speed.y;
};

Particle.prototype.setStackPos = function(pos) {
	this.stackPos = pos;
};

module.exports = {
	init: init
};