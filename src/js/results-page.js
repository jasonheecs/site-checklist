var checklistArticles;
var checkAllEl;
var allCheckedStatuses = {}; //object containing all the boolean flags to determine if all the checkboxes are checked for each checklist article

var forEach = Array.prototype.forEach; //alias to the forEach function in Array

/**
 * Check/Uncheck all the checklist items for a checklist article
 */
function setCheckAll(articleEl, areAllChecked) {
	var checkboxes = articleEl.querySelectorAll('.checklist__checkbox');
	var checkAllEls = articleEl.querySelectorAll('.checklist__check-all');

	forEach.call(checkAllEls, function(checkAllEl) {
		checkAllEl.parentNode.style.display = 'block'; //if user has js enabled, turn on the checkall
		checkAllEl.addEventListener('click', function(evt) {
			forEach.call(checkboxes, function(checkbox) { //check or uncheck all checkboxes
				checkbox.checked = !areAllChecked;
			});

			areAllChecked = !areAllChecked;
			setCheckAllText();
		});
	});


	forEach.call(checkboxes, function(checkbox) {
		checkbox.addEventListener('change', function(evt) {
			if (!checkbox.checked) {
				areAllChecked = false;
			}

			setCheckAllText();
		});
	});

	//toggle the inner text of the 'Check All' elements
	function setCheckAllText() {
		forEach.call(checkAllEls, function(checkAllEl) {
			checkAllEl.textContent = areAllChecked ? 'Uncheck All' : 'Check All';
		});
	}
}

/**
 * Set behaviour of add remarks button
 */
function setAddRemarks(articleEl) {
	var sections = articleEl.querySelectorAll('.checklist__item');

	forEach.call(sections, function(section) {
		var addRemarksEl = section.querySelector('.checklist__add-remarks');
		var remarksEl = section.querySelector('.remarks');

		addRemarksEl.style.display = 'block'; //enable the 'Add Remarks' btn if js is enabled
		remarksEl.classList.add('slide--in');
	});

	articleEl.addEventListener('click', function(evt) {
		// Add remarks button
		if (evt.target.classList.contains('checklist__add-remarks')) {
			var addRemarksEl = evt.target;
			var remarksEl = addRemarksEl.parentNode.nextElementSibling;
			addRemarksEl.classList.add('fade--out');
			remarksEl.classList.add('slide--out');
			remarksEl.classList.remove('slide--in');
			remarksEl.querySelector('.remarks__textarea').focus();			
		}

		// Remove remarks button
		if (evt.target.classList.contains('remarks__remove-btn')) {
			/* jshint shadow:true */
			var remarksEl = evt.target.parentNode;
			var addRemarksEl = remarksEl.parentNode.querySelector('.checklist__add-remarks');

			remarksEl.classList.remove('slide--out');
			remarksEl.classList.add('slide--in');
			remarksEl.querySelector('.remarks__textarea').value = '';

			addRemarksEl.classList.remove('fade--out');
			addRemarksEl.classList.add('fade--in');
		}
	});
}

function init() {
	checklistArticles = document.querySelectorAll('.checklist__article');
	forEach.call(checklistArticles, function(article, index) {
		allCheckedStatuses[index] = false;
		setCheckAll(article, allCheckedStatuses[index]);
		setAddRemarks(article);
	});
}

module.exports = {
	init: init
};