// http://youmightnotneedjquery.com/#matches_selector
var matches = function(el, selector) {
	return (el.matches || el.matchesSelector || el.msMatchesSelector || el.mozMatchesSelector || el.webkitMatchesSelector || el.oMatchesSelector).call(el, selector);
};

function hidePanel(panelEl) {
	panelEl.classList.remove('checklist__article--active');
	panelEl.classList.add('checklist__article--hide');
}

function showPanel(panelEl) {
	panelEl.classList.remove('checklist__article--hide');
	panelEl.classList.add('checklist__article--active');
}

module.exports = {
	init: function(navEl) {
		var activePanel;
		var activeLink = navEl.querySelector('.nav__link--active');
		var panels = document.querySelectorAll('.checklist__article');

		Array.prototype.forEach.call(panels, function(panel, index) {
			if (index === 0) {
				activePanel = panel;
				showPanel(activePanel);
			} else {
				hidePanel(panel);
			}
		});

		navEl.addEventListener('click', function(evt) {
			if (evt.target && evt.target.nodeName === "A") {
				evt.preventDefault();

				var linkEl = evt.target.parentNode; //element we are interested in is the parent of the a element (li)
				var link = evt.target.hash;

				if (link && !matches(linkEl, '.nav__link--active')) {
					hidePanel(activePanel);
					activeLink.classList.remove('nav__link--active');

					activePanel = document.getElementById(link.substring(1)); //remove '#' character
					activeLink = linkEl;

					showPanel(activePanel);
					activeLink.classList.add('nav__link--active');
				}
			}
		});
	}
};