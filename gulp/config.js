/**
 * 	Config file used for gulp tasks
 */

var build				= 'public'; //development folder (where website in development with unoptimised assets are located)
var src					= 'src'; //src folder (where various prebuilt assets are located)
var dist				= 'dist' //distribution folder (where final deployable website is located)

var cssFolderPath		= '/css/'; //where the css assets are located
var jsFolderPath		= '/js/'; //where the js assets are located

//TODO: Set browsersync production to tunnel server through public url

module.exports = {
	browsersync: {
		development: {
			proxy: 'http://192.168.33.10/', //proxy an existing vhost
			open: 'external', //Decide which URL to open automatically when Browsersync starts. Set to "local" for localhost URL
			port: 8888, //use a specific port, default port used by Browsersync is 3000
			files: [ //list of files to watch
				build + cssFolderPath + '*.css',
				build + jsFolderPath + '*.js',
				build + '/images/**'
			]
		},
		production: {
			base: dist, //dir to server files from
			proxy: 'localhost', //proxy an existing vhost
			open: 'local', //Decide which URL to open automatically when Browsersync starts. Set to "local" for localhost URL
			port: 9999 //use a specific port, default port used by Browsersync is 3000
		}
	},
	build: {
		production: {
			src: build + '/**/*/',
			dest: dist + '/'
		}
	},
	sass: {
		src: src + '/scss/**/*.{sass,scss}', //dir where the scss files reside
		dest: build + cssFolderPath, //dir to output the css file
		options: {
			outputStyle: 'expanded',
			sourceComments: true,
			sourceMap: './'
		}
	},
	autoprefixer: {
		browsers: [
			"last 3 versions"
		],
		cascade: true
	},
	browserify: {
		debug: true, //enable source maps
		extensions: ['.coffee', '.hbs'], //additional file extensions to make optional
		//a separate bundle will be generated for each bundle config below
		bundleConfigs: [
			{
				entries: './' + src + '/js/custom.js',
				dest: build + jsFolderPath,
				outputName: 'custom.js'
			}
		]
	},
	images: {
		src: [
			'!' + src + '/images/sprites/icons{,/**}', //ignore css sprites icons
			src + '/images/**/*',
		],
		dest: build + '/images'
	},
	base64: {
		src: build + cssFolderPath + '*.css',
		dest: build + cssFolderPath,
		options: {
			baseDir: 'public',
			extensions: ['png', 'jpg', 'jpeg', 'gif', 'svg'],
			maxImageSize: 20 * 1024, //bytes
			debug: true
		}
	},
	watch: {
		custom: [
			build + '/**/*.{php,html}'
		],
		sass: src + '/scss/**/*.{sass,scss}',
		scripts: src + '/js/**/*.js',
		images: src + '/images/**/*'
	},
	scsslint: {
		src: [
			src + '/scss/**/*.{sass,scss}',
			'!' + src + '/scss/_sprites.scss', //ignore generated sprites 
			'!' + src + '/scss/_sprites_jpg.scss'
		],
		options: {
			bundleExec: false
		}
	},
	jshint: {
		src: src + '/js/*.js'
	},
	sprites: {
		png: {
			src: src + '/images/sprites/icons/*.png',
			dest: {
				css: src + '/scss/',
				image: src + '/images/sprites/'
			},
			options: {
				cssName: '_sprites.scss',
				cssFormat: 'css',
				cssOpts: {
					cssSelector: function(item) {
						//if this is a hover sprite, name it as a hover one (e.g 'home-hover' -> 'home:hover')
						if (item.name.indexOf('-hover') !== -1) {
							return '.icon-' + item.name.replace('-hover', ':hover');
						} else { //otherwise, use name as selector
							return '.icon-' + item.name;
						}
					}
				},
				imgName: 'icon-sprite.png',
				imgPath: '/images/sprites/icon-sprite.png'
			}
		},
		jpg: {
			src: src + '/images/sprites/icons/*.{jpg,jpeg}',
			dest: {
				css: src + '/scss/',
				image: src + '/images/sprites/'
			},
			options: {
				cssName: '_sprites_jpg.scss',
				cssFormat: 'css',
				cssOpts: {
					cssSelector: function(item) {
						//if this is a hover sprite, name it as a hover one (e.g 'home-hover' -> 'home:hover')
						if (item.name.indexOf('-hover') !== -1) {
							return '.icon-' + item.name.replace('-hover', ':hover');
						} else { //otherwise, use name as selector
							return '.icon-' + item.name;
						}
					}
				},
				imgName: 'icon-sprite.jpg',
				imgPath: '/images/sprites/icon-sprite.jpg'
			}
		}
	},
	optimise: {
		css : {
			src: dist + cssFolderPath + '*.css',
			dest: dist + cssFolderPath,
			options: {
				autoprefixer: false
			}
		},
		js : {
			src: dist + jsFolderPath + '*.js',
			dest: dist + jsFolderPath
		},
		images: {
			src: dist + '/images/**/*.{jpg,jpeg,png,gif,svg}',
			dest: dist + '/images/',
			options: {
				interlaced: true
			},
			plugins: {
				png: {
					name: 'imagemin-pngquant',
					options: {
						quality: '70-80',
						speed: 1
					}
				},
				jpg: {
					name: 'imagemin-jpeg-recompress',
					options: {
						accurate: true,
						quality: 'high'
					}
				}
			}
		}
	},
	clean: {
		files: [
			dist + '/**/*',
			build + cssFolderPath + '*.map' //remove css sourcemaps
		]
	},
	revision: {
		src: {
			assets: [
				dist + cssFolderPath + '*.css',
				dist + jsFolderPath + '*.js'
			],
			base: dist
		},
		dest : {
			assets: dist,
			manifest: {
				name: 'manifest.json',
				path: dist
			}
		}
	},
	collect: {
		src: [
			dist + '/manifest.json',
			dist + '/**/*.{html,php,xml,json,css,js,txt}',
		],
		dest: dist
	},
	gzip: {
		src: dist + '/**/*.{css,js}',
		dest: dist,
		options: {}
	}
};