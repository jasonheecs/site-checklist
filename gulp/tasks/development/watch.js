/**
 * 	Start browsersync task and watch for file changes
 */

var gulp = require('gulp');
var config = require('../../config').watch;

gulp.task('watch', ['browsersync'], function() {
	gulp.watch(config.custom, ['rebuild']);
	gulp.watch(config.sass, ['sass', 'scsslint']);
	gulp.watch(config.scripts, ['scripts', 'jshint']);
	gulp.watch(config.images, ['images']);
	gulp.watch(config.sprites, ['sprites']);
});