/**
 * Cleans the development folder for any optimised files before starting development build
 * Dependencies:
 * 	- del
 */

var gulp = require('gulp');
var del = require('del');
var config = require('../../config').clean.development;

gulp.task('clean:development', function() {
	return del(config.files);
});