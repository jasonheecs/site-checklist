/**
 * 	Rebuilds and reloads browsersync
 * 	Dependecies:
 *  	- Browser-sync
 */

var gulp = require('gulp');
var browsersync = require('browser-sync');

gulp.task('rebuild', function() {
	browsersync.reload();
});