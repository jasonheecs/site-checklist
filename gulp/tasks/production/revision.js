/**
 * Revision all assets and write a manifest file
 * Dependencies:
 * 	- gulp-rev
 * 	- vinylPaths
 * 	- del
 */

var gulp = require('gulp');
var rev = require('gulp-rev');
var vinylPaths = require('vinyl-paths');
var del = require('del');
var config = require('../../config').revision;

gulp.task('revision', function() {
	return gulp.src(config.src.assets, {base: config.src.base})
		.pipe(vinylPaths(del)) //delete original files
		.pipe(gulp.dest(config.dest.assets + '/temp')) //copy original assets to tmp dir
		.pipe(rev())
		.pipe(gulp.dest(config.dest.assets)) //write rev'd assets to build dir
		.pipe(rev.manifest({path:config.dest.manifest.name}))
		.pipe(gulp.dest(config.dest.manifest.path)); //write manifest to build dir
});