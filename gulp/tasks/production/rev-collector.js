/**
 * Replace all links to assets in production files based on a manifest
 * Dependencies:
 * 	- gulp-rev-collector
 */

var gulp = require('gulp');
var collect = require('gulp-rev-collector');
var config = require('../../config').collect;

gulp.task('rev:collect', function() {
	return gulp.src(config.src)
		.pipe(collect())
		.pipe(gulp.dest(config.dest));
});