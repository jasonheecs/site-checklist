/**
 * Removes unwanted files for production build
 * Dependencies:
 * 	- del
 */

var gulp = require('gulp');
var del = require('del');
var config = require('../../config').clean;

gulp.task('clean:production', function() {
	return del(config.files);
});