/**
 * Clean up tmp files and folders after running revision tasks
 * Dependencies:
 * 	- del
 */

var gulp = require('gulp');
var del = require('del');
var config = require('../../config').revision;

gulp.task('rev:cleanup', function() {
	return del(config.dest.assets + '/temp');
});