/**
 * Start Browsersync server for production build with dist as base dir
 * Dependencies:
 *  - Browsersync
 *  - gul-connect-php
 */

var gulp = require('gulp');
var browsersync = require('browser-sync');
var connect = require('gulp-connect-php');
var config = require('../../config').browsersync.production;

gulp.task('browsersync:production', ['build:production'], function() {
	connect.server({
		base: config.base,
		hostname: config.proxy,
		port: config.port,
		keepalive: false
	}, function() {
		browsersync(config);
	});
});