/**
 * Run all tasks need for a production build
 * Dependencies:
 * 	- run-sequence
 */

var gulp = require('gulp');
var runSequence = require('run-sequence');
var config = require('../../config').build.production;

gulp.task('build:production', function(callback) {
	runSequence(
	'sprites',
	[
		'sass',
		'scripts',
		'images'
	],
	'base64',
	'clean:production',
	'build:production-copy',
	[
		'optimise:css',
		'optimise:js',
		'optimise:images'
	],
	'revision',
	'rev:collect',
	'rev:cleanup',
	'gzip',
	callback);
});

//copy all files from development folder to dist folder
gulp.task('build:production-copy', function() {
	return gulp.src(config.src).pipe(gulp.dest(config.dest));
});