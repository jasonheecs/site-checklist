<?php
namespace Models;

class Site {
	private $url; //url of site
	private $dom; //dom structure of site
	private $headers; //list of <h1> to <h6> tags
	private $links; //list of links
	private $page_title; //page title

	public function __construct($url) {
		$this->url = $url;
		$this->setDOM();
	}

	public function setDOM() {
		if (empty($this->url))
			throw new Exception('Site URL not defined!');

		$dom = new \DOMDocument();

		libxml_use_internal_errors(true);
		@$dom->loadHTML($this->retrieve_url_output($this->url));
		libxml_use_internal_errors(false);

		$this->dom = $dom;
	}

	public function getDOM() {
		return $this->dom;
	}

	public function getHeaderTags() {
		if (!$this->DOM_loaded())
			return;

		$xpath = new \DOMXPath($this->dom);
		$headers = $xpath->query("(//body//h1
								|//body//h2
								|//body//h3
								|//body//h4
								|//body//h5
								|//body//h6)");
		
		return $headers;
	}

	public function getLinks() {
		if (!$this->DOM_loaded())
			return;

		$this->links = $this->dom->getElementsByTagName('a');

		return $this->links;
	}

	public function getPageTitle() {
		if (!$this->DOM_loaded())
			return;

		$title_el = $this->dom->getElementsByTagName('title');
		if ($title_el->length)
			$this->page_title = $title_el->item(0)->textContent;

		return $this->page_title;
	}

	public function getMetaTags() {
		if (!$this->DOM_loaded())
			return;

		return $this->dom->getElementsByTagName('meta');
	}

	public function getURL() {
		return $this->url;
	}

	/**
	 * Check if DOM is an instance of DOMDocument
	 * @return [boolean]
	 */
	private function DOM_loaded() {
		if (!$this->dom instanceof \DOMDocument) {
			throw new \InvalidArgumentException('No DOMDocument loaded');
			return false;
		}

		return true;
	}

	/**
	 * Retrieve URL HTML output via curl
	 * @param  [string] $url [url of site]
	 * @return [string]      [HTML structure of site]
	 */
	private function retrieve_url_output($url) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0); 
		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, 60);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$output = curl_exec($ch);

		if (curl_errno($ch))
			echo 'Error connecting to site!';

		curl_close($ch);

		return $output;
	}
}