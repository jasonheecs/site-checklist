<?php
namespace Classes;

class Utility {
	/**
	 * Clean string to prevent xss
	 */
	public static function xss_clean($string) {
		return htmlspecialchars($string, ENT_QUOTES, 'UTF-8');
	}
}