<?php
namespace Controllers;

class ChecklistController {
	private $SiteChecklist;
	private $Site;
	const _STAGING_URL_ = '10.0.0.8';

	public function __construct($SiteChecklist, $site_url) {
		$this->Site = new \Models\Site($site_url);
		$this->SiteChecklist = $SiteChecklist;

		$this->init();
	}

	private function init() {
		$this->check_grammer_and_spelling();
		$this->list_of_header_tags();
		$this->check_staging_links();
		$this->get_page_title();
		$this->list_meta_tags();
	}

	public function check_grammer_and_spelling() {
		$html_el = $this->Site->getDOM()->getElementsByTagName('html')->item(0);

		//get the lang attribute from the HTML element, if any.
		if ($html_el->hasAttribute('lang'))
			$page_lang = $html_el->getAttribute('lang');

		//get body text and strip out comments and whitespaces
		$body_text = $this->Site->getDOM()->getElementsByTagName('body')->item(0)->textContent;
		$body_text = preg_replace('/<script(.*?)<\/script>/is', ' ', $body_text);
		$body_text = strip_tags($body_text);
		$body_text = preg_replace('/\s{2,}/u', ' ', $body_text);

		//Run body text against grammer and spell checker @ https://languagetool.org
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, 'https://languagetool.org:8081');
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POST, 1);
		if (isset($page_lang))
			curl_setopt($ch, CURLOPT_POSTFIELDS, 'language=' . $page_lang . '&text=' . $body_text);
		else
			curl_setopt($ch, CURLOPT_POSTFIELDS, 'autodetect=1&text=' . $body_text);
		curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, 60);
		$output = curl_exec($ch);

		if (curl_errno($ch))
			echo 'Unable to check spelling and grammar';

		curl_close($ch);

		$this->SiteChecklist->setLanguageErrors($output);
	}

	/**
	 * Get list of header tags
	 */
	public function list_of_header_tags() {
		$header_tags = $this->Site->getHeaderTags();
		$headers_list = array();

		foreach ($header_tags as $header) {
			$headers_list[$header->tagName][] = trim($header->nodeValue);
		}

		ksort($headers_list); //sort array by keys

		$this->SiteChecklist->setHeaders($headers_list);
	}

	/**
	 * Check if there are still links pointing to staging environment
	 */
	public function check_staging_links() {
		$links = $this->Site->getLinks();
		$staging_links = array();

		foreach ($links as $link) {
			if ($href = $this->get_link_href($link)) {
				if (strpos($href, 'localhost') || strpos($href, self::_STAGING_URL_))
					$staging_links[] = $link->getAttribute('href');
			}
		}

		$this->SiteChecklist->setStagingLinks($staging_links);
	}

	/**
	 * Get title of web page
	 */
	public function get_page_title() {
		$this->SiteChecklist->setPageTitle($this->Site->getPageTitle());
	}

	/**
	 * List a unique array list of all the links on the page
	 */
	public function list_links() {
		$links = $this->Site->getLinks();
		$links_list = array();

		foreach ($links as $link) {
			if ($href = $this->get_link_href($link))
				$links_list[] = $href;
		}

		return array_unique($links_list);
	}

	/**
	 * List all meta tags on page
	 */
	public function list_meta_tags() {
		$meta_tags = $this->Site->getMetaTags();
		$meta_list = array();

		foreach ($meta_tags as $meta_tag) {
			$tmp_dom = new \DOMDocument();
			$tmp_dom->appendChild($tmp_dom->importNode($meta_tag, false));
			$meta_list[] = $tmp_dom->saveHTML($tmp_dom);
		}

		$this->SiteChecklist->setMetaData($meta_list);
	}

	/**
	 * Get the href attribute of a link Node element, if the attribute exists
	 * @param  [NodeList] $link_el 
	 * @return [String/boolean]          [the href attribute if it exists, false if it doesn't]
	 */
	private function get_link_href($link_el) {
		if ($link_el->hasAttribute('href'))
			return $link_el->getAttribute('href');

		return false;
	}

	public function getSite() {
		return $this->Site;
	}
}