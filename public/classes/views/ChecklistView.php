<?php
namespace Views;

class ChecklistView {
	private $SiteChecklist;
	private $ChecklistController;

	public function __construct($ChecklistController, $SiteChecklist) {
		$this->ChecklistController = $ChecklistController;
		$this->SiteChecklist = $SiteChecklist;
	}

	public function output() {
		$site_url = $this->ChecklistController->getSite()->getURL();
		$language_errors = $this->process_language_errors();
		$header_tags = $this->process_header_tags();
		$staging_links = $this->process_staging_links();
		$page_title = $this->SiteChecklist->getPageTitle();
		$list_of_links = $this->process_list_of_links();
		$list_of_meta_tags = $this->process_meta_tags();

		//view data to be passed to template
		$data = array(
			'content' => array(
				'title' => 'Content and Style',
				'sections' => array(
					array(
						'title' => 'Typography and layout'
					),
					array(
						'title' => 'Check for incorrect punctuation marks, paragraphings, etc'
					),
					array(
						'title' => 'Obvious Spelling and Grammer',
						'details' => $language_errors
					)
				)
			),
			'consistency' => array(
				'title' => 'Consistency',
				'sections' => array(
					array(
						'title' => 'Capitalization (especially of main headings)',
						'details' => $header_tags
					),
					array(
						'title' => 'Recurring/common phrases (e.g. ‘More about X’ links).'
					),
					array(
						'title' => 'Headers, sub headers with reference sizing to other parts of website'
					),
					array(
						'title' => 'Variations in words (e.g. Websites vs Web Sites, or UK vs US spelling).'
					),
					array(
						'title' => 'Treatment of bulleted lists (e.g. periods or commas at end of each item).'
					),
					array(
						'title' => 'Check for hard-coded links to staging domain (i.e. links to localhost)',
						'details' => $staging_links
					),
					array(
						'title' => 'Ensure no test content on site.'
					),
					array(
						'title' => 'Check how important pages (e.g. content items) print.'
					),
					array(
						'title' => 'Ensure important old/existing URLs are redirected to relevant new URLs, if the URL scheme is changing'
					)
				)
			),
			'seo' => array(
				'title' => 'Search Engine Visibility, SEO and Metrics',
				'sections' => array(
					array(
						'title' => 'Page Titles are important; ensure they make sense and have relevant keywords in them.',
						'details' => 'Page Title is: ' . $page_title
					),
					array(
						'title' => 'Create metadata descriptions for important pages. Clients can do the rest',
						'details' => $list_of_meta_tags
					),
					array(
						'title' => 'All access should have www prefix as default.'
					),
					array(
						'title' => 'Ensure content is marked-up semantically/correctly (h1, etc.).'
					),
					array(
						'title' => 'Check for target keyword usage in general content.'
					),
					array(
						'title' => 'Create an XML Sitemap and link to Google Webmaster tool'
					),
					array(
						'title' => 'Check format (user/search engine friendliness) of URLs.',
						'details' => $list_of_links
					)
				)
			),
			'functional' => array(
				'title' => 'Functional Testing',
				'sections' => array(
					array(
						'title' => 'Check all bespoke/complex functionality. Check from INVOICE'
					),
					array(
						'title' => 'Check search functionality (including relevance of results).'
					),
					array(
						'title' => 'Check on common variations of browser (Internet Explorer, Firefox, Safari, Chrome etc.)'
					),
					array(
						'title' => 'Check on common variations of Screen Resolution.'
					),
					array(
						'title' => 'Test all forms and emailing (e.g. contact us, blog comments), including anti-spam, response emails, etc.'
					),
					array(
						'title' => 'Check all external links are valid. Including Social media links'
					)
				)
			),
			'functional-ecommerce' => array(
				'title' => 'Functional Testing (Ecommerce)',
				'sections' => array(
					array(
						'title' => 'Creation of default account with password efu987'
					),
					array(
						'title' => 'Add 10 items to cart and proceed all the way to checkout confirmation page without obvious issues'
					),
					array(
						'title' => 'Create inactive and permanent $0.10 item for testing'
					),
					array(
						'title' => 'Do a LIVE payment transaction for each payment gateway on $0.10 item'
					),
					array(
						'title' => 'Check shipping calculation for local and overseas'
					),
					array(
						'title' => 'Check final total calculation with TAX, SHIPPING, DISCOUNT with correct amount in gateways'
					),
					array(
						'title' => 'Facebook or socal network login'
					),
					array(
						'title' => 'Insert BN code for paypal'
					),
					array(
						'title' => 'Check administrator receives all system emails'
					)
				)
			),
			'security' => array(
				'title' => 'Security and Risk',
				'sections' => array(
					array(
						'title' => 'Prepare backup procedures, backup and leave BK file in server root, ask client to download'
					),
					array(
						'title' => 'Protect sensitive pages (e.g. administration area).'
					),
					array(
						'title' => 'Check not using lazy id and passwords'
					),
					array(
						'title' => 'Check disk space/capacity.'
					)
				)
			),
			'finishing' => array(
				'title' => 'Finishing Touches',
				'sections' => array(
					array(
						'title' => 'Create custom 404/error pages.'
					),
					array(
						'title' => 'Google map for contact us page'
					),
					array(
						'title' => 'Create a favicon'
					),
					array(
						'title' => 'Insert Powered By Efusion link or <Meta generator> tag'
					)
				)
			),
			'accessibility' => array(
				'title' => 'Accessibility',
				'sections' => array(
					array(
						'title' => '"Alt" attributes used for all descriptive images'
					),
					array(
						'title' => 'High contrast color used everywhere'
					),
					array(
						'title' => 'Color and size used for critical information'
					),
					array(
						'title' => 'Tested on most common browsers'
					),
					array(
						'title' => 'Tested on mobile devices'
					)
				)
			),
			'post-launch' => array(
				'title' => 'Post Launch',
				'sections' => array(
					array(
						'title' => 'Social Media Marketing: Twitter, LinkedIn, Digg, Facebook, Stumbleupon, etc.'
					),
					array(
						'title' => 'Submit to google webmaster tool (efusion or client\'s)'
					),
					array(
						'title' => 'Check formatting of site results in SERPs.'
					),
					array(
						'title' => 'Invite client to write testimonial'
					),
					array(
						'title' => 'Added to portfolio'
					),
					array(
						'title' => 'Invite client to join Emall.sg (ecommerce)'
					),
					array(
						'title' => 'Send Client Emarketing guide (check with WK if too outdated)'
					),
					array(
						'title' => 'Enable content versioning for Joomla'
					),
					array(
						'title' => 'Pass woorank index > 60 pts'
					)
				)
			)
		);

		$_SESSION['view_data'] = $data;

		require_once($this->SiteChecklist->template);
	}

	/**
	 * Create HTML / DOM structure for the language errors output to be used in template
	 * @return [string] 
	 */
	private function process_language_errors() {
		$language_errors = $this->SiteChecklist->getLanguageErrors();

		if (!$language_errors) //if empty array
			return '';

		$dom = new \DOMDocument();

		$dom->appendChild($dom->createElement('h3', key($language_errors)));

		foreach ($language_errors as $errorType => $errors) {
			$table = $dom->createElement('table');

			$tr = $dom->createElement('tr');
			$tr->appendChild($dom->createElement('th', 'Context'));
			$tr->appendChild($dom->createElement('th', 'Suggested Replacements'));

			$table->appendChild($tr);

			foreach ($errors as $error) {
				$tr = $dom->createElement('tr');
				$tr->appendChild($dom->createElement('td', $error['context']));
				$tr->appendChild($dom->createElement('td', implode(", ", $error['replacements'])));
				$table->appendChild($tr);
			}
			$dom->appendChild($table);
		}

		return $dom->saveHTML();
	}

	/**
	 * Create HTML / DOM structure for the list of headers to be used in template
	 * @return [string]
	 */
	private function process_header_tags() {
		$headers = $this->SiteChecklist->getHeaders();

		if (!$headers)
			return '';

		$dom = new \DOMDocument();
		$dom->appendChild($dom->createElement('h3', 'List of headers'));

		$table = $dom->createElement('table');

		$tr = $dom->createElement('tr');
		$tr->appendChild($dom->createElement('th', 'Tag'));
		$tr->appendChild($dom->createElement('th', 'Tag Content'));
		$table->appendChild($tr);

		foreach ($headers as $tag => $header) {
			$tr = $dom->createElement('tr');
			$tr->appendChild($dom->createElement('td', $tag));

			$frag = $dom->createDocumentFragment();
			$frag->appendXML('<td>' . implode('<br/>', $header) . '</td>');
			$tr->appendChild($frag);

			$table->appendChild($tr);
		}		
		$dom->appendChild($table);

		return $dom->saveHTML();
	}

	/**
	 * Create HTML / DOM structure for the list of staging links to be used in template
	 * @return [string]
	 */
	private function process_staging_links() {
		$staging_links = $this->SiteChecklist->getStagingLinks();

		if (!$staging_links)
			return '';

		$dom = new \DOMDocument();
		$dom->appendChild($dom->createElement('h3', 'List of possible staging links'));
		$dom->appendChild($this->generateSimpleHTMLTable($dom, $staging_links));

		return $dom->saveHTML();
	}

	/**
	 * Create HTML / DOM structure for the list of links to be used in template
	 * @return [string]
	 */
	private function process_list_of_links() {
		$list_of_links = $this->ChecklistController->list_links();

		if (!$list_of_links)
			return '';

		$dom = new \DOMDocument();
		$dom->appendChild($dom->createElement('h3', 'List of links on page'));
		$dom->appendChild($this->generateSimpleHTMLTable($dom, $list_of_links));

		return $dom->saveHTML();
	}

	/**
	 * Create HTML / DOM structure for the list of meta tags to be used in template
	 * @return [string]
	 */
	private function process_meta_tags() {
		$meta_tags = $this->SiteChecklist->getMetaData();

		if (!$meta_tags)
			return '';

		$dom = new \DOMDocument();
		$dom->appendChild($dom->createElement('h3', 'List of meta data on page'));
		$dom->appendChild($this->generateSimpleHTMLTable($dom, $meta_tags));

		return $dom->saveHTML();
	}

	/**
	 * Generates a simple one-column Table DOMElement
	 * @return [type] [description]
	 */
	private function generateSimpleHTMLTable($dom, $data) {
		$table = $dom->createElement('table');

		foreach ($data as $row_data) {
			$tr = $dom->createElement('tr');
			$tr->appendChild($dom->createElement('td', $row_data));
			$table->appendChild($tr);
		}

		return $table;
	}
}