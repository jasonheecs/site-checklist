<?php
namespace Views;

class IndexView {
	private $IndexModel;
	private $IndexController;

	public function __construct($IndexController, $IndexModel) {
		$this->IndexController = $IndexController;
		$this->IndexModel = $IndexModel;
	}

	public function output($error_messages) {
		require_once($this->IndexModel->template);
	}
}