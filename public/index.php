<?php
session_start();
require('Psr4AutoloaderClass.php');

$loader = new Psr4AutoloaderClass; // instantiate the loader

// add namespaces; /classes as root namespace
$loader->addNamespace('Classes', dirname(__FILE__) . '/classes');
$loader->addNamespace('Controllers', dirname(__FILE__) . '/classes/controllers');
$loader->addNamespace('Models', dirname(__FILE__) . '/classes/models');
$loader->addNamespace('Views', dirname(__FILE__) . '/classes/views');

// register the autoloader
$loader->register();

/**
 * Check if a url string is in a valid format
 * @param  string  $url 
 * @return boolean
 */
function is_valid_url($url) {
	if ((filter_var($url, FILTER_VALIDATE_URL) !== false) && 
		(parse_url($url, PHP_URL_HOST) !== 'http') &&
		(parse_url($url, PHP_URL_HOST) !== 'https'))
			return true;

	return false;
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Site Checklist</title>
	<link rel="stylesheet" src="//normalize-css.googlecode.com/svn/trunk/normalize.min.css" />
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300,500,700,900' rel='stylesheet' type='text/css'>
	<link href='//maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="css/custom.css">
</head>
<body>
	<?php
	$page = 'home'; //default page is homepage
	$errors = array(); // error messages
	$site_url = '';

	if (isset($_GET['site']) && !empty($_GET['site'])) {
		$site_url = \Classes\Utility::xss_clean($_GET['site']);

		if (strpos($site_url, 'http') === false && strpos($site_url, 'https') === false) {
			$site_url = 'http://' . $site_url;
		}

		if (is_valid_url($site_url)) {
			$page = 'checklist';
		} else {
			$errors[] = 'The URL ' . $site_url . ' is invalid.';
		}
	}

	//list of mvcs
	$data = array(
		'home' => array('model' => 'IndexModel', 'view' => 'IndexView', 'controller' => 'IndexController'),
		'checklist' => array('model' => 'SiteChecklist', 'view' => 'ChecklistView', 'controller' => 'ChecklistController')
	);

	foreach ($data as $key => $components) {
		if ($page == $key) {
			$model = '\Models\\' . $components['model'];
			$view = '\Views\\' . $components['view'];
			$controller = '\Controllers\\' . $components['controller'];
			break;
		}
	}

	if (isset($model)) {
		$m = new $model();
		$c = new $controller($m, $site_url);
		$v = new $view($c, $m);
		echo $v->output($errors);
	}
	?>
	<script src="js/custom.js"></script>
</body>
</html>