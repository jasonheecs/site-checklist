(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
var particles = require('./particles');
var nav = require('./nav');
var resultsPage = require('./results-page');

document.addEventListener('DOMContentLoaded', function() {
	if (document.querySelector('.lp__container'))
		particles.init(document.querySelector('.lp__container'), {});

	if (document.getElementById('nav'))
		nav.init(document.getElementById('nav'));

	if (document.getElementById('results-page'))
		resultsPage.init();
});
},{"./nav":2,"./particles":3,"./results-page":4}],2:[function(require,module,exports){
// http://youmightnotneedjquery.com/#matches_selector
var matches = function(el, selector) {
	return (el.matches || el.matchesSelector || el.msMatchesSelector || el.mozMatchesSelector || el.webkitMatchesSelector || el.oMatchesSelector).call(el, selector);
};

function hidePanel(panelEl) {
	panelEl.classList.remove('checklist__article--active');
	panelEl.classList.add('checklist__article--hide');
}

function showPanel(panelEl) {
	panelEl.classList.remove('checklist__article--hide');
	panelEl.classList.add('checklist__article--active');
}

module.exports = {
	init: function(navEl) {
		var activePanel;
		var activeLink = navEl.querySelector('.nav__link--active');
		var panels = document.querySelectorAll('.checklist__article');

		Array.prototype.forEach.call(panels, function(panel, index) {
			if (index === 0) {
				activePanel = panel;
				showPanel(activePanel);
			} else {
				hidePanel(panel);
			}
		});

		navEl.addEventListener('click', function(evt) {
			if (evt.target && evt.target.nodeName === "A") {
				evt.preventDefault();

				var linkEl = evt.target.parentNode; //element we are interested in is the parent of the a element (li)
				var link = evt.target.hash;

				if (link && !matches(linkEl, '.nav__link--active')) {
					hidePanel(activePanel);
					activeLink.classList.remove('nav__link--active');

					activePanel = document.getElementById(link.substring(1)); //remove '#' character
					activeLink = linkEl;

					showPanel(activePanel);
					activeLink.classList.add('nav__link--active');
				}
			}
		});
	}
};
},{}],3:[function(require,module,exports){
/*jshint node: true */
/*globals document*/
/*globals window*/
"use strict";

// http://youmightnotneedjquery.com/#deep_extend
function extend(out) {
	out = out || {};

	for (var i = 1; i < arguments.length; i++) {
		var obj = arguments[i];

		if (!obj)
		  continue;

		for (var key in obj) {
		  if (obj.hasOwnProperty(key)) {
		    if (typeof obj[key] === 'object')
		      out[key] = extend(out[key], obj[key]);
		    else
		      out[key] = obj[key];
		  }
		}
	}

	return out;
}

/**
 * function to throttle js calls
 */
function throttle(callback, limit) {
	var wait = false;
	return function() {
		if (!wait) {
			callback.call();
			wait = true;
			setTimeout(function() {
				wait = false;
			}, limit);
		}
	};
}

var element;
var canvasSupport = !!document.createElement('canvas').getContext;
var canvas;
var ctx;
var particles = [];
var raf;
var options; //specified options via init
//default options
var defaults = {
	minSpeedX: 0.1,
	maxSpeedX: 0.7,
	minSpeedY: 0.1,
	maxSpeedY: 0.7,
	density: 10000, // How many particles will be generated: one particle every n pixels
	dotColor: 'rgba(102,102,102,0.35)',
	lineColor: 'rgba(102,102,102,0.35)',
	particleRadius: 7, //dot size
	lineWidth: 1,
	proximity: 100 // How close two dots need to be before they join
};

function init(_element, _options) {
	options = extend({}, defaults, _options);
	element = _element;

	if (!canvasSupport)
		return;

	//create canvas
	canvas = document.createElement('canvas');
	canvas.className = 'particles-canvas';
	canvas.style.display = 'block';
	element.insertBefore(canvas, element.firstChild);
	ctx = canvas.getContext('2d');
	styleCanvas(element, canvas);

	//create particles
	var numParticles = Math.round((canvas.width * canvas.height) / options.density);
	for (var i = 0; i < numParticles; i++) {
		var p = new Particle();
		p.setStackPos(i);
		particles.push(p);
	}

	window.addEventListener('resize', throttle(resizeHandler, 150));

	draw();
}

/**
 * Style the canvas
 */
function styleCanvas(element, canvas) {
	canvas.width = element.offsetWidth;
	canvas.height = element.offsetHeight;
	ctx.fillStyle = options.dotColor;
	ctx.strokeStyle = options.lineColor;
	ctx.lineWidth = options.lineWidth;
}

/**
 * Add/remove particles on window resize
 */
function resizeHandler() {
	//resize the canvas
	styleCanvas(element, canvas);

	var elWidth = element.offsetWidth;
	var elHeight = element.offsetHeight;

	//remove particles outside the canvas
	for (var i = particles.length - 1; i >=0; i--) {
		if (particles[i].position.x > elWidth || particles[i].position.y > elHeight)
			particles.splice(i, 1);
	}

	//adjust particles density
	var numParticles = Math.round((canvas.width * canvas.height) / options.density);
	if (numParticles > particles.length) {
		while (numParticles > particles.length) {
			var p = new Particle();
			particles.push(p);
		}
	} else if (numParticles < particles.length) {
		particles.splice(numParticles);
	}

	//reindex particles
	particles.forEach(function(particle, i) {
		particle.setStackPos(i);
	});
}

function draw() {
	//wipe canvas
	ctx.clearRect(0, 0 , canvas.width, canvas.height);

	//update particle positions and draw
	particles.forEach(function(particle) {
		particle.updatePosition();
		particle.draw();
	});

	window.requestAnimationFrame(draw);
}

/**
 * Particle object
 */
function Particle() {
	this.stackPos = 0;
	this.active = true;
	this.layer = Math.ceil(Math.random() * 3);
	//initial particle position
	this.position = {
		x: Math.ceil(Math.random() * canvas.width),
		y: Math.ceil(Math.random() * canvas.height)
	};

	//Random particle speed, within min and max values
	this.speed = {};
	this.speed.x = +((-options.maxSpeedX / 2) + (Math.random() * options.maxSpeedX)).toFixed(2);
	this.speed.x += this.speed.x > 0 ? options.minSpeedX : -options.minSpeedX;
	this.speed.y = +((-options.maxSpeedY / 2) + (Math.random() * options.maxSpeedY)).toFixed(2);
	this.speed.y += this.speed.y > 0 ? options.minSpeedY: -options.minSpeedY;
}

Particle.prototype.draw = function() {
	//draw circle
	ctx.beginPath();
	ctx.arc(this.position.x, this.position.y, options.particleRadius / 2, 0, Math.PI * 2, true);
	ctx.closePath();
	ctx.fill();

	//draw lines
	ctx.beginPath();
	//iterate over all particles which are higher in the stack than this one
	for (var i = particles.length - 1; i > this.stackPos; i--) {
		var p2 = particles[i];

		//Pythagorus theorum to get distance between 2 pts
		var a = this.position.x - p2.position.x;
		var b = this.position.y - p2.position.y;
		var dist = Math.sqrt((a * a) + (b * b)).toFixed(2);

		//if two particles are within proximity, join them
		if (dist < options.proximity) {
			ctx.moveTo(this.position.x, this.position.y);
			ctx.lineTo(p2.position.x, p2.position.y);
		}
	}
	
	ctx.stroke();
	ctx.closePath();
};

Particle.prototype.updatePosition = function() {
	var elWidth = element.offsetWidth;
	var elHeight = element.offsetHeight;

	//if particle has reached edge of canvas
	if (this.position.y + this.speed.y > elHeight || this.position.y + this.speed.y < 0)
		this.speed.y = -this.speed.y;

	if (this.position.x > elWidth) {
		this.position.x = 0;
	}
	else if (this.position.x < 0) {
		this.position.x = elWidth;
	}

	//move particle
	this.position.x += this.speed.x;
	this.position.y += this.speed.y;
};

Particle.prototype.setStackPos = function(pos) {
	this.stackPos = pos;
};

module.exports = {
	init: init
};
},{}],4:[function(require,module,exports){
var checklistArticles;
var checkAllEl;
var allCheckedStatuses = {}; //object containing all the boolean flags to determine if all the checkboxes are checked for each checklist article

var forEach = Array.prototype.forEach; //alias to the forEach function in Array

/**
 * Check/Uncheck all the checklist items for a checklist article
 */
function setCheckAll(articleEl, areAllChecked) {
	var checkboxes = articleEl.querySelectorAll('.checklist__checkbox');
	var checkAllEls = articleEl.querySelectorAll('.checklist__check-all');

	forEach.call(checkAllEls, function(checkAllEl) {
		checkAllEl.parentNode.style.display = 'block'; //if user has js enabled, turn on the checkall
		checkAllEl.addEventListener('click', function(evt) {
			forEach.call(checkboxes, function(checkbox) { //check or uncheck all checkboxes
				checkbox.checked = !areAllChecked;
			});

			areAllChecked = !areAllChecked;
			setCheckAllText();
		});
	});


	forEach.call(checkboxes, function(checkbox) {
		checkbox.addEventListener('change', function(evt) {
			if (!checkbox.checked) {
				areAllChecked = false;
			}

			setCheckAllText();
		});
	});

	//toggle the inner text of the 'Check All' elements
	function setCheckAllText() {
		forEach.call(checkAllEls, function(checkAllEl) {
			checkAllEl.textContent = areAllChecked ? 'Uncheck All' : 'Check All';
		});
	}
}

/**
 * Set behaviour of add remarks button
 */
function setAddRemarks(articleEl) {
	var sections = articleEl.querySelectorAll('.checklist__item');

	forEach.call(sections, function(section) {
		var addRemarksEl = section.querySelector('.checklist__add-remarks');
		var remarksEl = section.querySelector('.remarks');

		addRemarksEl.style.display = 'block'; //enable the 'Add Remarks' btn if js is enabled
		remarksEl.classList.add('slide--in');
	});

	articleEl.addEventListener('click', function(evt) {
		// Add remarks button
		if (evt.target.classList.contains('checklist__add-remarks')) {
			var addRemarksEl = evt.target;
			var remarksEl = addRemarksEl.parentNode.nextElementSibling;
			addRemarksEl.classList.add('fade--out');
			remarksEl.classList.add('slide--out');
			remarksEl.classList.remove('slide--in');
			remarksEl.querySelector('.remarks__textarea').focus();			
		}

		// Remove remarks button
		if (evt.target.classList.contains('remarks__remove-btn')) {
			/* jshint shadow:true */
			var remarksEl = evt.target.parentNode;
			var addRemarksEl = remarksEl.parentNode.querySelector('.checklist__add-remarks');

			remarksEl.classList.remove('slide--out');
			remarksEl.classList.add('slide--in');
			remarksEl.querySelector('.remarks__textarea').value = '';

			addRemarksEl.classList.remove('fade--out');
			addRemarksEl.classList.add('fade--in');
		}
	});
}

function init() {
	checklistArticles = document.querySelectorAll('.checklist__article');
	forEach.call(checklistArticles, function(article, index) {
		allCheckedStatuses[index] = false;
		setCheckAll(article, allCheckedStatuses[index]);
		setAddRemarks(article);
	});
}

module.exports = {
	init: init
};
},{}]},{},[1])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyaWZ5L25vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJzcmMvanMvY3VzdG9tLmpzIiwic3JjL2pzL25hdi5qcyIsInNyYy9qcy9wYXJ0aWNsZXMuanMiLCJzcmMvanMvcmVzdWx0cy1wYWdlLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FDQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNiQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDbERBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDbE9BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwiZmlsZSI6ImdlbmVyYXRlZC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzQ29udGVudCI6WyIoZnVuY3Rpb24gZSh0LG4scil7ZnVuY3Rpb24gcyhvLHUpe2lmKCFuW29dKXtpZighdFtvXSl7dmFyIGE9dHlwZW9mIHJlcXVpcmU9PVwiZnVuY3Rpb25cIiYmcmVxdWlyZTtpZighdSYmYSlyZXR1cm4gYShvLCEwKTtpZihpKXJldHVybiBpKG8sITApO3ZhciBmPW5ldyBFcnJvcihcIkNhbm5vdCBmaW5kIG1vZHVsZSAnXCIrbytcIidcIik7dGhyb3cgZi5jb2RlPVwiTU9EVUxFX05PVF9GT1VORFwiLGZ9dmFyIGw9bltvXT17ZXhwb3J0czp7fX07dFtvXVswXS5jYWxsKGwuZXhwb3J0cyxmdW5jdGlvbihlKXt2YXIgbj10W29dWzFdW2VdO3JldHVybiBzKG4/bjplKX0sbCxsLmV4cG9ydHMsZSx0LG4scil9cmV0dXJuIG5bb10uZXhwb3J0c312YXIgaT10eXBlb2YgcmVxdWlyZT09XCJmdW5jdGlvblwiJiZyZXF1aXJlO2Zvcih2YXIgbz0wO288ci5sZW5ndGg7bysrKXMocltvXSk7cmV0dXJuIHN9KSIsInZhciBwYXJ0aWNsZXMgPSByZXF1aXJlKCcuL3BhcnRpY2xlcycpO1xudmFyIG5hdiA9IHJlcXVpcmUoJy4vbmF2Jyk7XG52YXIgcmVzdWx0c1BhZ2UgPSByZXF1aXJlKCcuL3Jlc3VsdHMtcGFnZScpO1xuXG5kb2N1bWVudC5hZGRFdmVudExpc3RlbmVyKCdET01Db250ZW50TG9hZGVkJywgZnVuY3Rpb24oKSB7XG5cdGlmIChkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcubHBfX2NvbnRhaW5lcicpKVxuXHRcdHBhcnRpY2xlcy5pbml0KGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5scF9fY29udGFpbmVyJyksIHt9KTtcblxuXHRpZiAoZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ25hdicpKVxuXHRcdG5hdi5pbml0KGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCduYXYnKSk7XG5cblx0aWYgKGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdyZXN1bHRzLXBhZ2UnKSlcblx0XHRyZXN1bHRzUGFnZS5pbml0KCk7XG59KTsiLCIvLyBodHRwOi8veW91bWlnaHRub3RuZWVkanF1ZXJ5LmNvbS8jbWF0Y2hlc19zZWxlY3RvclxudmFyIG1hdGNoZXMgPSBmdW5jdGlvbihlbCwgc2VsZWN0b3IpIHtcblx0cmV0dXJuIChlbC5tYXRjaGVzIHx8IGVsLm1hdGNoZXNTZWxlY3RvciB8fCBlbC5tc01hdGNoZXNTZWxlY3RvciB8fCBlbC5tb3pNYXRjaGVzU2VsZWN0b3IgfHwgZWwud2Via2l0TWF0Y2hlc1NlbGVjdG9yIHx8IGVsLm9NYXRjaGVzU2VsZWN0b3IpLmNhbGwoZWwsIHNlbGVjdG9yKTtcbn07XG5cbmZ1bmN0aW9uIGhpZGVQYW5lbChwYW5lbEVsKSB7XG5cdHBhbmVsRWwuY2xhc3NMaXN0LnJlbW92ZSgnY2hlY2tsaXN0X19hcnRpY2xlLS1hY3RpdmUnKTtcblx0cGFuZWxFbC5jbGFzc0xpc3QuYWRkKCdjaGVja2xpc3RfX2FydGljbGUtLWhpZGUnKTtcbn1cblxuZnVuY3Rpb24gc2hvd1BhbmVsKHBhbmVsRWwpIHtcblx0cGFuZWxFbC5jbGFzc0xpc3QucmVtb3ZlKCdjaGVja2xpc3RfX2FydGljbGUtLWhpZGUnKTtcblx0cGFuZWxFbC5jbGFzc0xpc3QuYWRkKCdjaGVja2xpc3RfX2FydGljbGUtLWFjdGl2ZScpO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IHtcblx0aW5pdDogZnVuY3Rpb24obmF2RWwpIHtcblx0XHR2YXIgYWN0aXZlUGFuZWw7XG5cdFx0dmFyIGFjdGl2ZUxpbmsgPSBuYXZFbC5xdWVyeVNlbGVjdG9yKCcubmF2X19saW5rLS1hY3RpdmUnKTtcblx0XHR2YXIgcGFuZWxzID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCgnLmNoZWNrbGlzdF9fYXJ0aWNsZScpO1xuXG5cdFx0QXJyYXkucHJvdG90eXBlLmZvckVhY2guY2FsbChwYW5lbHMsIGZ1bmN0aW9uKHBhbmVsLCBpbmRleCkge1xuXHRcdFx0aWYgKGluZGV4ID09PSAwKSB7XG5cdFx0XHRcdGFjdGl2ZVBhbmVsID0gcGFuZWw7XG5cdFx0XHRcdHNob3dQYW5lbChhY3RpdmVQYW5lbCk7XG5cdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRoaWRlUGFuZWwocGFuZWwpO1xuXHRcdFx0fVxuXHRcdH0pO1xuXG5cdFx0bmF2RWwuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCBmdW5jdGlvbihldnQpIHtcblx0XHRcdGlmIChldnQudGFyZ2V0ICYmIGV2dC50YXJnZXQubm9kZU5hbWUgPT09IFwiQVwiKSB7XG5cdFx0XHRcdGV2dC5wcmV2ZW50RGVmYXVsdCgpO1xuXG5cdFx0XHRcdHZhciBsaW5rRWwgPSBldnQudGFyZ2V0LnBhcmVudE5vZGU7IC8vZWxlbWVudCB3ZSBhcmUgaW50ZXJlc3RlZCBpbiBpcyB0aGUgcGFyZW50IG9mIHRoZSBhIGVsZW1lbnQgKGxpKVxuXHRcdFx0XHR2YXIgbGluayA9IGV2dC50YXJnZXQuaGFzaDtcblxuXHRcdFx0XHRpZiAobGluayAmJiAhbWF0Y2hlcyhsaW5rRWwsICcubmF2X19saW5rLS1hY3RpdmUnKSkge1xuXHRcdFx0XHRcdGhpZGVQYW5lbChhY3RpdmVQYW5lbCk7XG5cdFx0XHRcdFx0YWN0aXZlTGluay5jbGFzc0xpc3QucmVtb3ZlKCduYXZfX2xpbmstLWFjdGl2ZScpO1xuXG5cdFx0XHRcdFx0YWN0aXZlUGFuZWwgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChsaW5rLnN1YnN0cmluZygxKSk7IC8vcmVtb3ZlICcjJyBjaGFyYWN0ZXJcblx0XHRcdFx0XHRhY3RpdmVMaW5rID0gbGlua0VsO1xuXG5cdFx0XHRcdFx0c2hvd1BhbmVsKGFjdGl2ZVBhbmVsKTtcblx0XHRcdFx0XHRhY3RpdmVMaW5rLmNsYXNzTGlzdC5hZGQoJ25hdl9fbGluay0tYWN0aXZlJyk7XG5cdFx0XHRcdH1cblx0XHRcdH1cblx0XHR9KTtcblx0fVxufTsiLCIvKmpzaGludCBub2RlOiB0cnVlICovXG4vKmdsb2JhbHMgZG9jdW1lbnQqL1xuLypnbG9iYWxzIHdpbmRvdyovXG5cInVzZSBzdHJpY3RcIjtcblxuLy8gaHR0cDovL3lvdW1pZ2h0bm90bmVlZGpxdWVyeS5jb20vI2RlZXBfZXh0ZW5kXG5mdW5jdGlvbiBleHRlbmQob3V0KSB7XG5cdG91dCA9IG91dCB8fCB7fTtcblxuXHRmb3IgKHZhciBpID0gMTsgaSA8IGFyZ3VtZW50cy5sZW5ndGg7IGkrKykge1xuXHRcdHZhciBvYmogPSBhcmd1bWVudHNbaV07XG5cblx0XHRpZiAoIW9iailcblx0XHQgIGNvbnRpbnVlO1xuXG5cdFx0Zm9yICh2YXIga2V5IGluIG9iaikge1xuXHRcdCAgaWYgKG9iai5oYXNPd25Qcm9wZXJ0eShrZXkpKSB7XG5cdFx0ICAgIGlmICh0eXBlb2Ygb2JqW2tleV0gPT09ICdvYmplY3QnKVxuXHRcdCAgICAgIG91dFtrZXldID0gZXh0ZW5kKG91dFtrZXldLCBvYmpba2V5XSk7XG5cdFx0ICAgIGVsc2Vcblx0XHQgICAgICBvdXRba2V5XSA9IG9ialtrZXldO1xuXHRcdCAgfVxuXHRcdH1cblx0fVxuXG5cdHJldHVybiBvdXQ7XG59XG5cbi8qKlxuICogZnVuY3Rpb24gdG8gdGhyb3R0bGUganMgY2FsbHNcbiAqL1xuZnVuY3Rpb24gdGhyb3R0bGUoY2FsbGJhY2ssIGxpbWl0KSB7XG5cdHZhciB3YWl0ID0gZmFsc2U7XG5cdHJldHVybiBmdW5jdGlvbigpIHtcblx0XHRpZiAoIXdhaXQpIHtcblx0XHRcdGNhbGxiYWNrLmNhbGwoKTtcblx0XHRcdHdhaXQgPSB0cnVlO1xuXHRcdFx0c2V0VGltZW91dChmdW5jdGlvbigpIHtcblx0XHRcdFx0d2FpdCA9IGZhbHNlO1xuXHRcdFx0fSwgbGltaXQpO1xuXHRcdH1cblx0fTtcbn1cblxudmFyIGVsZW1lbnQ7XG52YXIgY2FudmFzU3VwcG9ydCA9ICEhZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnY2FudmFzJykuZ2V0Q29udGV4dDtcbnZhciBjYW52YXM7XG52YXIgY3R4O1xudmFyIHBhcnRpY2xlcyA9IFtdO1xudmFyIHJhZjtcbnZhciBvcHRpb25zOyAvL3NwZWNpZmllZCBvcHRpb25zIHZpYSBpbml0XG4vL2RlZmF1bHQgb3B0aW9uc1xudmFyIGRlZmF1bHRzID0ge1xuXHRtaW5TcGVlZFg6IDAuMSxcblx0bWF4U3BlZWRYOiAwLjcsXG5cdG1pblNwZWVkWTogMC4xLFxuXHRtYXhTcGVlZFk6IDAuNyxcblx0ZGVuc2l0eTogMTAwMDAsIC8vIEhvdyBtYW55IHBhcnRpY2xlcyB3aWxsIGJlIGdlbmVyYXRlZDogb25lIHBhcnRpY2xlIGV2ZXJ5IG4gcGl4ZWxzXG5cdGRvdENvbG9yOiAncmdiYSgxMDIsMTAyLDEwMiwwLjM1KScsXG5cdGxpbmVDb2xvcjogJ3JnYmEoMTAyLDEwMiwxMDIsMC4zNSknLFxuXHRwYXJ0aWNsZVJhZGl1czogNywgLy9kb3Qgc2l6ZVxuXHRsaW5lV2lkdGg6IDEsXG5cdHByb3hpbWl0eTogMTAwIC8vIEhvdyBjbG9zZSB0d28gZG90cyBuZWVkIHRvIGJlIGJlZm9yZSB0aGV5IGpvaW5cbn07XG5cbmZ1bmN0aW9uIGluaXQoX2VsZW1lbnQsIF9vcHRpb25zKSB7XG5cdG9wdGlvbnMgPSBleHRlbmQoe30sIGRlZmF1bHRzLCBfb3B0aW9ucyk7XG5cdGVsZW1lbnQgPSBfZWxlbWVudDtcblxuXHRpZiAoIWNhbnZhc1N1cHBvcnQpXG5cdFx0cmV0dXJuO1xuXG5cdC8vY3JlYXRlIGNhbnZhc1xuXHRjYW52YXMgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdjYW52YXMnKTtcblx0Y2FudmFzLmNsYXNzTmFtZSA9ICdwYXJ0aWNsZXMtY2FudmFzJztcblx0Y2FudmFzLnN0eWxlLmRpc3BsYXkgPSAnYmxvY2snO1xuXHRlbGVtZW50Lmluc2VydEJlZm9yZShjYW52YXMsIGVsZW1lbnQuZmlyc3RDaGlsZCk7XG5cdGN0eCA9IGNhbnZhcy5nZXRDb250ZXh0KCcyZCcpO1xuXHRzdHlsZUNhbnZhcyhlbGVtZW50LCBjYW52YXMpO1xuXG5cdC8vY3JlYXRlIHBhcnRpY2xlc1xuXHR2YXIgbnVtUGFydGljbGVzID0gTWF0aC5yb3VuZCgoY2FudmFzLndpZHRoICogY2FudmFzLmhlaWdodCkgLyBvcHRpb25zLmRlbnNpdHkpO1xuXHRmb3IgKHZhciBpID0gMDsgaSA8IG51bVBhcnRpY2xlczsgaSsrKSB7XG5cdFx0dmFyIHAgPSBuZXcgUGFydGljbGUoKTtcblx0XHRwLnNldFN0YWNrUG9zKGkpO1xuXHRcdHBhcnRpY2xlcy5wdXNoKHApO1xuXHR9XG5cblx0d2luZG93LmFkZEV2ZW50TGlzdGVuZXIoJ3Jlc2l6ZScsIHRocm90dGxlKHJlc2l6ZUhhbmRsZXIsIDE1MCkpO1xuXG5cdGRyYXcoKTtcbn1cblxuLyoqXG4gKiBTdHlsZSB0aGUgY2FudmFzXG4gKi9cbmZ1bmN0aW9uIHN0eWxlQ2FudmFzKGVsZW1lbnQsIGNhbnZhcykge1xuXHRjYW52YXMud2lkdGggPSBlbGVtZW50Lm9mZnNldFdpZHRoO1xuXHRjYW52YXMuaGVpZ2h0ID0gZWxlbWVudC5vZmZzZXRIZWlnaHQ7XG5cdGN0eC5maWxsU3R5bGUgPSBvcHRpb25zLmRvdENvbG9yO1xuXHRjdHguc3Ryb2tlU3R5bGUgPSBvcHRpb25zLmxpbmVDb2xvcjtcblx0Y3R4LmxpbmVXaWR0aCA9IG9wdGlvbnMubGluZVdpZHRoO1xufVxuXG4vKipcbiAqIEFkZC9yZW1vdmUgcGFydGljbGVzIG9uIHdpbmRvdyByZXNpemVcbiAqL1xuZnVuY3Rpb24gcmVzaXplSGFuZGxlcigpIHtcblx0Ly9yZXNpemUgdGhlIGNhbnZhc1xuXHRzdHlsZUNhbnZhcyhlbGVtZW50LCBjYW52YXMpO1xuXG5cdHZhciBlbFdpZHRoID0gZWxlbWVudC5vZmZzZXRXaWR0aDtcblx0dmFyIGVsSGVpZ2h0ID0gZWxlbWVudC5vZmZzZXRIZWlnaHQ7XG5cblx0Ly9yZW1vdmUgcGFydGljbGVzIG91dHNpZGUgdGhlIGNhbnZhc1xuXHRmb3IgKHZhciBpID0gcGFydGljbGVzLmxlbmd0aCAtIDE7IGkgPj0wOyBpLS0pIHtcblx0XHRpZiAocGFydGljbGVzW2ldLnBvc2l0aW9uLnggPiBlbFdpZHRoIHx8IHBhcnRpY2xlc1tpXS5wb3NpdGlvbi55ID4gZWxIZWlnaHQpXG5cdFx0XHRwYXJ0aWNsZXMuc3BsaWNlKGksIDEpO1xuXHR9XG5cblx0Ly9hZGp1c3QgcGFydGljbGVzIGRlbnNpdHlcblx0dmFyIG51bVBhcnRpY2xlcyA9IE1hdGgucm91bmQoKGNhbnZhcy53aWR0aCAqIGNhbnZhcy5oZWlnaHQpIC8gb3B0aW9ucy5kZW5zaXR5KTtcblx0aWYgKG51bVBhcnRpY2xlcyA+IHBhcnRpY2xlcy5sZW5ndGgpIHtcblx0XHR3aGlsZSAobnVtUGFydGljbGVzID4gcGFydGljbGVzLmxlbmd0aCkge1xuXHRcdFx0dmFyIHAgPSBuZXcgUGFydGljbGUoKTtcblx0XHRcdHBhcnRpY2xlcy5wdXNoKHApO1xuXHRcdH1cblx0fSBlbHNlIGlmIChudW1QYXJ0aWNsZXMgPCBwYXJ0aWNsZXMubGVuZ3RoKSB7XG5cdFx0cGFydGljbGVzLnNwbGljZShudW1QYXJ0aWNsZXMpO1xuXHR9XG5cblx0Ly9yZWluZGV4IHBhcnRpY2xlc1xuXHRwYXJ0aWNsZXMuZm9yRWFjaChmdW5jdGlvbihwYXJ0aWNsZSwgaSkge1xuXHRcdHBhcnRpY2xlLnNldFN0YWNrUG9zKGkpO1xuXHR9KTtcbn1cblxuZnVuY3Rpb24gZHJhdygpIHtcblx0Ly93aXBlIGNhbnZhc1xuXHRjdHguY2xlYXJSZWN0KDAsIDAgLCBjYW52YXMud2lkdGgsIGNhbnZhcy5oZWlnaHQpO1xuXG5cdC8vdXBkYXRlIHBhcnRpY2xlIHBvc2l0aW9ucyBhbmQgZHJhd1xuXHRwYXJ0aWNsZXMuZm9yRWFjaChmdW5jdGlvbihwYXJ0aWNsZSkge1xuXHRcdHBhcnRpY2xlLnVwZGF0ZVBvc2l0aW9uKCk7XG5cdFx0cGFydGljbGUuZHJhdygpO1xuXHR9KTtcblxuXHR3aW5kb3cucmVxdWVzdEFuaW1hdGlvbkZyYW1lKGRyYXcpO1xufVxuXG4vKipcbiAqIFBhcnRpY2xlIG9iamVjdFxuICovXG5mdW5jdGlvbiBQYXJ0aWNsZSgpIHtcblx0dGhpcy5zdGFja1BvcyA9IDA7XG5cdHRoaXMuYWN0aXZlID0gdHJ1ZTtcblx0dGhpcy5sYXllciA9IE1hdGguY2VpbChNYXRoLnJhbmRvbSgpICogMyk7XG5cdC8vaW5pdGlhbCBwYXJ0aWNsZSBwb3NpdGlvblxuXHR0aGlzLnBvc2l0aW9uID0ge1xuXHRcdHg6IE1hdGguY2VpbChNYXRoLnJhbmRvbSgpICogY2FudmFzLndpZHRoKSxcblx0XHR5OiBNYXRoLmNlaWwoTWF0aC5yYW5kb20oKSAqIGNhbnZhcy5oZWlnaHQpXG5cdH07XG5cblx0Ly9SYW5kb20gcGFydGljbGUgc3BlZWQsIHdpdGhpbiBtaW4gYW5kIG1heCB2YWx1ZXNcblx0dGhpcy5zcGVlZCA9IHt9O1xuXHR0aGlzLnNwZWVkLnggPSArKCgtb3B0aW9ucy5tYXhTcGVlZFggLyAyKSArIChNYXRoLnJhbmRvbSgpICogb3B0aW9ucy5tYXhTcGVlZFgpKS50b0ZpeGVkKDIpO1xuXHR0aGlzLnNwZWVkLnggKz0gdGhpcy5zcGVlZC54ID4gMCA/IG9wdGlvbnMubWluU3BlZWRYIDogLW9wdGlvbnMubWluU3BlZWRYO1xuXHR0aGlzLnNwZWVkLnkgPSArKCgtb3B0aW9ucy5tYXhTcGVlZFkgLyAyKSArIChNYXRoLnJhbmRvbSgpICogb3B0aW9ucy5tYXhTcGVlZFkpKS50b0ZpeGVkKDIpO1xuXHR0aGlzLnNwZWVkLnkgKz0gdGhpcy5zcGVlZC55ID4gMCA/IG9wdGlvbnMubWluU3BlZWRZOiAtb3B0aW9ucy5taW5TcGVlZFk7XG59XG5cblBhcnRpY2xlLnByb3RvdHlwZS5kcmF3ID0gZnVuY3Rpb24oKSB7XG5cdC8vZHJhdyBjaXJjbGVcblx0Y3R4LmJlZ2luUGF0aCgpO1xuXHRjdHguYXJjKHRoaXMucG9zaXRpb24ueCwgdGhpcy5wb3NpdGlvbi55LCBvcHRpb25zLnBhcnRpY2xlUmFkaXVzIC8gMiwgMCwgTWF0aC5QSSAqIDIsIHRydWUpO1xuXHRjdHguY2xvc2VQYXRoKCk7XG5cdGN0eC5maWxsKCk7XG5cblx0Ly9kcmF3IGxpbmVzXG5cdGN0eC5iZWdpblBhdGgoKTtcblx0Ly9pdGVyYXRlIG92ZXIgYWxsIHBhcnRpY2xlcyB3aGljaCBhcmUgaGlnaGVyIGluIHRoZSBzdGFjayB0aGFuIHRoaXMgb25lXG5cdGZvciAodmFyIGkgPSBwYXJ0aWNsZXMubGVuZ3RoIC0gMTsgaSA+IHRoaXMuc3RhY2tQb3M7IGktLSkge1xuXHRcdHZhciBwMiA9IHBhcnRpY2xlc1tpXTtcblxuXHRcdC8vUHl0aGFnb3J1cyB0aGVvcnVtIHRvIGdldCBkaXN0YW5jZSBiZXR3ZWVuIDIgcHRzXG5cdFx0dmFyIGEgPSB0aGlzLnBvc2l0aW9uLnggLSBwMi5wb3NpdGlvbi54O1xuXHRcdHZhciBiID0gdGhpcy5wb3NpdGlvbi55IC0gcDIucG9zaXRpb24ueTtcblx0XHR2YXIgZGlzdCA9IE1hdGguc3FydCgoYSAqIGEpICsgKGIgKiBiKSkudG9GaXhlZCgyKTtcblxuXHRcdC8vaWYgdHdvIHBhcnRpY2xlcyBhcmUgd2l0aGluIHByb3hpbWl0eSwgam9pbiB0aGVtXG5cdFx0aWYgKGRpc3QgPCBvcHRpb25zLnByb3hpbWl0eSkge1xuXHRcdFx0Y3R4Lm1vdmVUbyh0aGlzLnBvc2l0aW9uLngsIHRoaXMucG9zaXRpb24ueSk7XG5cdFx0XHRjdHgubGluZVRvKHAyLnBvc2l0aW9uLngsIHAyLnBvc2l0aW9uLnkpO1xuXHRcdH1cblx0fVxuXHRcblx0Y3R4LnN0cm9rZSgpO1xuXHRjdHguY2xvc2VQYXRoKCk7XG59O1xuXG5QYXJ0aWNsZS5wcm90b3R5cGUudXBkYXRlUG9zaXRpb24gPSBmdW5jdGlvbigpIHtcblx0dmFyIGVsV2lkdGggPSBlbGVtZW50Lm9mZnNldFdpZHRoO1xuXHR2YXIgZWxIZWlnaHQgPSBlbGVtZW50Lm9mZnNldEhlaWdodDtcblxuXHQvL2lmIHBhcnRpY2xlIGhhcyByZWFjaGVkIGVkZ2Ugb2YgY2FudmFzXG5cdGlmICh0aGlzLnBvc2l0aW9uLnkgKyB0aGlzLnNwZWVkLnkgPiBlbEhlaWdodCB8fCB0aGlzLnBvc2l0aW9uLnkgKyB0aGlzLnNwZWVkLnkgPCAwKVxuXHRcdHRoaXMuc3BlZWQueSA9IC10aGlzLnNwZWVkLnk7XG5cblx0aWYgKHRoaXMucG9zaXRpb24ueCA+IGVsV2lkdGgpIHtcblx0XHR0aGlzLnBvc2l0aW9uLnggPSAwO1xuXHR9XG5cdGVsc2UgaWYgKHRoaXMucG9zaXRpb24ueCA8IDApIHtcblx0XHR0aGlzLnBvc2l0aW9uLnggPSBlbFdpZHRoO1xuXHR9XG5cblx0Ly9tb3ZlIHBhcnRpY2xlXG5cdHRoaXMucG9zaXRpb24ueCArPSB0aGlzLnNwZWVkLng7XG5cdHRoaXMucG9zaXRpb24ueSArPSB0aGlzLnNwZWVkLnk7XG59O1xuXG5QYXJ0aWNsZS5wcm90b3R5cGUuc2V0U3RhY2tQb3MgPSBmdW5jdGlvbihwb3MpIHtcblx0dGhpcy5zdGFja1BvcyA9IHBvcztcbn07XG5cbm1vZHVsZS5leHBvcnRzID0ge1xuXHRpbml0OiBpbml0XG59OyIsInZhciBjaGVja2xpc3RBcnRpY2xlcztcbnZhciBjaGVja0FsbEVsO1xudmFyIGFsbENoZWNrZWRTdGF0dXNlcyA9IHt9OyAvL29iamVjdCBjb250YWluaW5nIGFsbCB0aGUgYm9vbGVhbiBmbGFncyB0byBkZXRlcm1pbmUgaWYgYWxsIHRoZSBjaGVja2JveGVzIGFyZSBjaGVja2VkIGZvciBlYWNoIGNoZWNrbGlzdCBhcnRpY2xlXG5cbnZhciBmb3JFYWNoID0gQXJyYXkucHJvdG90eXBlLmZvckVhY2g7IC8vYWxpYXMgdG8gdGhlIGZvckVhY2ggZnVuY3Rpb24gaW4gQXJyYXlcblxuLyoqXG4gKiBDaGVjay9VbmNoZWNrIGFsbCB0aGUgY2hlY2tsaXN0IGl0ZW1zIGZvciBhIGNoZWNrbGlzdCBhcnRpY2xlXG4gKi9cbmZ1bmN0aW9uIHNldENoZWNrQWxsKGFydGljbGVFbCwgYXJlQWxsQ2hlY2tlZCkge1xuXHR2YXIgY2hlY2tib3hlcyA9IGFydGljbGVFbC5xdWVyeVNlbGVjdG9yQWxsKCcuY2hlY2tsaXN0X19jaGVja2JveCcpO1xuXHR2YXIgY2hlY2tBbGxFbHMgPSBhcnRpY2xlRWwucXVlcnlTZWxlY3RvckFsbCgnLmNoZWNrbGlzdF9fY2hlY2stYWxsJyk7XG5cblx0Zm9yRWFjaC5jYWxsKGNoZWNrQWxsRWxzLCBmdW5jdGlvbihjaGVja0FsbEVsKSB7XG5cdFx0Y2hlY2tBbGxFbC5wYXJlbnROb2RlLnN0eWxlLmRpc3BsYXkgPSAnYmxvY2snOyAvL2lmIHVzZXIgaGFzIGpzIGVuYWJsZWQsIHR1cm4gb24gdGhlIGNoZWNrYWxsXG5cdFx0Y2hlY2tBbGxFbC5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIGZ1bmN0aW9uKGV2dCkge1xuXHRcdFx0Zm9yRWFjaC5jYWxsKGNoZWNrYm94ZXMsIGZ1bmN0aW9uKGNoZWNrYm94KSB7IC8vY2hlY2sgb3IgdW5jaGVjayBhbGwgY2hlY2tib3hlc1xuXHRcdFx0XHRjaGVja2JveC5jaGVja2VkID0gIWFyZUFsbENoZWNrZWQ7XG5cdFx0XHR9KTtcblxuXHRcdFx0YXJlQWxsQ2hlY2tlZCA9ICFhcmVBbGxDaGVja2VkO1xuXHRcdFx0c2V0Q2hlY2tBbGxUZXh0KCk7XG5cdFx0fSk7XG5cdH0pO1xuXG5cblx0Zm9yRWFjaC5jYWxsKGNoZWNrYm94ZXMsIGZ1bmN0aW9uKGNoZWNrYm94KSB7XG5cdFx0Y2hlY2tib3guYWRkRXZlbnRMaXN0ZW5lcignY2hhbmdlJywgZnVuY3Rpb24oZXZ0KSB7XG5cdFx0XHRpZiAoIWNoZWNrYm94LmNoZWNrZWQpIHtcblx0XHRcdFx0YXJlQWxsQ2hlY2tlZCA9IGZhbHNlO1xuXHRcdFx0fVxuXG5cdFx0XHRzZXRDaGVja0FsbFRleHQoKTtcblx0XHR9KTtcblx0fSk7XG5cblx0Ly90b2dnbGUgdGhlIGlubmVyIHRleHQgb2YgdGhlICdDaGVjayBBbGwnIGVsZW1lbnRzXG5cdGZ1bmN0aW9uIHNldENoZWNrQWxsVGV4dCgpIHtcblx0XHRmb3JFYWNoLmNhbGwoY2hlY2tBbGxFbHMsIGZ1bmN0aW9uKGNoZWNrQWxsRWwpIHtcblx0XHRcdGNoZWNrQWxsRWwudGV4dENvbnRlbnQgPSBhcmVBbGxDaGVja2VkID8gJ1VuY2hlY2sgQWxsJyA6ICdDaGVjayBBbGwnO1xuXHRcdH0pO1xuXHR9XG59XG5cbi8qKlxuICogU2V0IGJlaGF2aW91ciBvZiBhZGQgcmVtYXJrcyBidXR0b25cbiAqL1xuZnVuY3Rpb24gc2V0QWRkUmVtYXJrcyhhcnRpY2xlRWwpIHtcblx0dmFyIHNlY3Rpb25zID0gYXJ0aWNsZUVsLnF1ZXJ5U2VsZWN0b3JBbGwoJy5jaGVja2xpc3RfX2l0ZW0nKTtcblxuXHRmb3JFYWNoLmNhbGwoc2VjdGlvbnMsIGZ1bmN0aW9uKHNlY3Rpb24pIHtcblx0XHR2YXIgYWRkUmVtYXJrc0VsID0gc2VjdGlvbi5xdWVyeVNlbGVjdG9yKCcuY2hlY2tsaXN0X19hZGQtcmVtYXJrcycpO1xuXHRcdHZhciByZW1hcmtzRWwgPSBzZWN0aW9uLnF1ZXJ5U2VsZWN0b3IoJy5yZW1hcmtzJyk7XG5cblx0XHRhZGRSZW1hcmtzRWwuc3R5bGUuZGlzcGxheSA9ICdibG9jayc7IC8vZW5hYmxlIHRoZSAnQWRkIFJlbWFya3MnIGJ0biBpZiBqcyBpcyBlbmFibGVkXG5cdFx0cmVtYXJrc0VsLmNsYXNzTGlzdC5hZGQoJ3NsaWRlLS1pbicpO1xuXHR9KTtcblxuXHRhcnRpY2xlRWwuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCBmdW5jdGlvbihldnQpIHtcblx0XHQvLyBBZGQgcmVtYXJrcyBidXR0b25cblx0XHRpZiAoZXZ0LnRhcmdldC5jbGFzc0xpc3QuY29udGFpbnMoJ2NoZWNrbGlzdF9fYWRkLXJlbWFya3MnKSkge1xuXHRcdFx0dmFyIGFkZFJlbWFya3NFbCA9IGV2dC50YXJnZXQ7XG5cdFx0XHR2YXIgcmVtYXJrc0VsID0gYWRkUmVtYXJrc0VsLnBhcmVudE5vZGUubmV4dEVsZW1lbnRTaWJsaW5nO1xuXHRcdFx0YWRkUmVtYXJrc0VsLmNsYXNzTGlzdC5hZGQoJ2ZhZGUtLW91dCcpO1xuXHRcdFx0cmVtYXJrc0VsLmNsYXNzTGlzdC5hZGQoJ3NsaWRlLS1vdXQnKTtcblx0XHRcdHJlbWFya3NFbC5jbGFzc0xpc3QucmVtb3ZlKCdzbGlkZS0taW4nKTtcblx0XHRcdHJlbWFya3NFbC5xdWVyeVNlbGVjdG9yKCcucmVtYXJrc19fdGV4dGFyZWEnKS5mb2N1cygpO1x0XHRcdFxuXHRcdH1cblxuXHRcdC8vIFJlbW92ZSByZW1hcmtzIGJ1dHRvblxuXHRcdGlmIChldnQudGFyZ2V0LmNsYXNzTGlzdC5jb250YWlucygncmVtYXJrc19fcmVtb3ZlLWJ0bicpKSB7XG5cdFx0XHQvKiBqc2hpbnQgc2hhZG93OnRydWUgKi9cblx0XHRcdHZhciByZW1hcmtzRWwgPSBldnQudGFyZ2V0LnBhcmVudE5vZGU7XG5cdFx0XHR2YXIgYWRkUmVtYXJrc0VsID0gcmVtYXJrc0VsLnBhcmVudE5vZGUucXVlcnlTZWxlY3RvcignLmNoZWNrbGlzdF9fYWRkLXJlbWFya3MnKTtcblxuXHRcdFx0cmVtYXJrc0VsLmNsYXNzTGlzdC5yZW1vdmUoJ3NsaWRlLS1vdXQnKTtcblx0XHRcdHJlbWFya3NFbC5jbGFzc0xpc3QuYWRkKCdzbGlkZS0taW4nKTtcblx0XHRcdHJlbWFya3NFbC5xdWVyeVNlbGVjdG9yKCcucmVtYXJrc19fdGV4dGFyZWEnKS52YWx1ZSA9ICcnO1xuXG5cdFx0XHRhZGRSZW1hcmtzRWwuY2xhc3NMaXN0LnJlbW92ZSgnZmFkZS0tb3V0Jyk7XG5cdFx0XHRhZGRSZW1hcmtzRWwuY2xhc3NMaXN0LmFkZCgnZmFkZS0taW4nKTtcblx0XHR9XG5cdH0pO1xufVxuXG5mdW5jdGlvbiBpbml0KCkge1xuXHRjaGVja2xpc3RBcnRpY2xlcyA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoJy5jaGVja2xpc3RfX2FydGljbGUnKTtcblx0Zm9yRWFjaC5jYWxsKGNoZWNrbGlzdEFydGljbGVzLCBmdW5jdGlvbihhcnRpY2xlLCBpbmRleCkge1xuXHRcdGFsbENoZWNrZWRTdGF0dXNlc1tpbmRleF0gPSBmYWxzZTtcblx0XHRzZXRDaGVja0FsbChhcnRpY2xlLCBhbGxDaGVja2VkU3RhdHVzZXNbaW5kZXhdKTtcblx0XHRzZXRBZGRSZW1hcmtzKGFydGljbGUpO1xuXHR9KTtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSB7XG5cdGluaXQ6IGluaXRcbn07Il19
