<main id="results-page">
	<form action="pdf.php" method="POST" target="_blank">
		<aside class="text--center">
			<nav class="text--left">
				<ul id="nav" class="nav">
					<li class="nav__link nav__link--active">
						<a href="#content"><i class="fa fa-clipboard"></i>Content and Style</a>
					</li>
					<li class="nav__link">
						<a href="#consistency"><i class="fa fa-align-left"></i>Consistency</a>
					</li>
					<li class="nav__link">
						<a href="#seo"><i class="fa fa-google"></i>SEO</a>
					</li>
					<li class="nav__link">
						<a href="#functional"><i class="fa fa-list-alt"></i>Functional Testing</a>
					</li>
					<li class="nav__link">
						<a href="#functional-ecommerce"><i class="fa fa-shopping-cart"></i>Functional Testing (Ecommerce)</a>
					</li>
					<li class="nav__link">
						<a href="#security"><i class="fa fa-user-secret"></i>Security and Risk</a>
					</li>
					<li class="nav__link">
						<a href="#finishing"><i class="fa fa-magic"></i>Finishing Touches</a>
					</li>
					<li class="nav__link">
						<a href="#accessibility"><i class="fa fa-wheelchair"></i>Accessibility</a>
					</li>
					<li class="nav__link">
						<a href="#post-launch"><i class="fa fa-thumbs-up"></i>Post Launch</a>
					</li>
				</ul>
			</nav>
			<button type="submit" class="btn btn--primary">Export as PDF</button>
		</aside>
		<div id="checklist-results" class="checklist">
			<h4 class="checklist__site-title text--right"><a href="<?=$site_url;?>" target="_blank"><?=$site_url;?></a></h4>
			<input type="hidden" name="site_url" value="<?=$site_url;?>" />
			<?php
			foreach ($data as $key => $value) :
			?>

			<article id="<?=$key;?>" class="checklist__article">
				<h1 class="checklist__article-title"><?=$value['title'];?></h1>
				<?php foreach ($value['sections'] as $index => $section) : ?>
				<?php 
				$form_id_prefix = $key.'['.$index.']';
				?>

				<section class="checklist__item">
					<h2 class="checklist__item-title"><?=$section['title'];?></h2>
					<div class="checklist__item-details"><?=array_key_exists('details', $section) ? $section['details'] : '';?></div>
					<div class="checklist__item-cta clearfix">
						<button type="button" class="checklist__add-remarks btn btn--secondary float-left fade" style="display:none;">Add Remarks</button>
						<div class="checkbox__container float-right">
							<input id="<?=$form_id_prefix.'[checkbox]';?>" name="<?=$form_id_prefix.'[checkbox]';?>" type="checkbox" class="checklist__checkbox" />
							<label for="<?=$form_id_prefix.'[checkbox]';?>" class="checklist__checkbox-label"></label>
						</div>
					</div>
					<div class="remarks slide">
						<textarea name="<?=$form_id_prefix.'[remarks]';?>" class="remarks__textarea" rows="10" cols="150" placeholder="Enter Remarks"></textarea>
						<button type="button" class="remarks__remove-btn btn btn--teritary" title="Remove Remarks"><i class="fa fa-trash"></i></button>
					</div>
				</section>
				<?php endforeach; ?>
				<div class="checklist__check-all-container text--right" style="display:none"><span class="checklist__check-all">Check all</span></div>
			</article>
			<?php endforeach; ?>
		</div>
	</form>
</main>