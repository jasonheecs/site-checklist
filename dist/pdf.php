<?php
require_once('fpdf/fpdf.php');
require_once('Psr4AutoloaderClass.php');

$loader = new Psr4AutoloaderClass; // instantiate the loader
$loader->addNamespace('Classes', dirname(__FILE__) . '/classes');
$loader->register();

use \Classes\Utility as Utility;

session_start();
date_default_timezone_set('UTC'); 

if (!isset($_POST['site_url']) || !isset($_SESSION['view_data'])) {
	die('No data to export');
}

$view_data = $_SESSION['view_data'];
$post_data = $_POST;

class PDF extends FPDF {
	const TABLE_CELL_HEIGHT = 3;
	const MARGINS = 10;
	const BODY_FONT_SIZE = 8;

	public function __construct() {
		parent::__construct();

		$this->SetMargins(self::MARGINS, self::MARGINS);
		$this->SetFont('Helvetica','', self::BODY_FONT_SIZE);
	}

	function Header() {
		$this->SetFont('Helvetica','',6);
		$this->Cell(0, 10, 'Date: '. date('d/m/Y'), 0, 1, 'R');
	}

	function AddTable($data, $site_url) {
		$page_width = $this->GetPageWidth() - ($this::MARGINS * 2);
		$widths = array((95 / 100) * $page_width, (5 / 100) * $page_width); //column widths(% of $page_width)

		//Site name and link
		$this->SetFont('Helvetica','B',12);
		// $this->Cell(0, 10, 'Checklist for http://www.efusiontech.com', 0, 1);
		$this->Write(10, 'Checklist for ');
		$this->SetTextColor(0,0,255);
		$this->Write(10, $site_url, $site_url);
		$this->SetTextColor(0);
		$this->Ln();

		foreach ($data as $article) {
			$this->SetFont('Helvetica','B', 9);
			$this->Cell(0, self::TABLE_CELL_HEIGHT * 1.5, $article['title'], 0);
			$this->SetFont('Helvetica','', self::BODY_FONT_SIZE);

			$this->Ln();

			foreach ($article['sections'] as $section) {
				$this->SetFont('Helvetica','', self::BODY_FONT_SIZE);
				$this->Cell($widths[0], self::TABLE_CELL_HEIGHT * 2, $section['title'], 'LT');

				if ($section['checked']) { //check mark
					$this->SetFont('ZapfDingbats','', 16);
					$this->Cell($widths[1], self::TABLE_CELL_HEIGHT * 2, chr(52), 'RT', 0, 'R');
				} else {
					$this->Cell($widths[1], self::TABLE_CELL_HEIGHT * 2, '', 'RT', 0, 'R');
				}

				$this->Ln();

				if (isset($section['remarks'])) { //remarks
					$this->SetFont('Helvetica','B', self::BODY_FONT_SIZE - 1);
					$this->Cell(0, self::TABLE_CELL_HEIGHT, 'Remarks:', 'LR', 2);
					$this->SetFont('Helvetica','', self::BODY_FONT_SIZE - 1);
					$this->MultiCell(0, self::TABLE_CELL_HEIGHT, $section['remarks'], 'LR', 'L');
				}				
			}

			// Closing line
			$this->Cell(0, 0, '', 'T', 1);
			$this->Cell(0, self::TABLE_CELL_HEIGHT, '', 0);
			$this->Ln();
		}		
	}

	function Footer() {
		$this->SetY(-15); //position 1.5cm from bottom
		$this->Cell(0, 10, 'Page '.$this->PageNo() . '/{nb}', 0, 0, 'C');
	}
}

$data = array();

foreach ($view_data as $key => $article) {
	$data[$key]['title'] = $article['title'];

	foreach ($article['sections'] as $index => $section) {
		$section_data = array();
		$section_data['title'] = $section['title'];

		if (isset($post_data[$key][$index]['checkbox']) && $post_data[$key][$index]['checkbox'] == 'on') {
			$section_data['checked'] = true;
		} else {
			$section_data['checked'] = false;
		}

		if (isset($post_data[$key][$index]['remarks']) && $post_data[$key][$index]['remarks'] !== '') {
			$section_data['remarks'] = Utility::xss_clean($post_data[$key][$index]['remarks']);
		}

		$data[$key]['sections'][] = $section_data;
	}
}

$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->AddTable($data, Utility::xss_clean($post_data['site_url']));
$pdf->Output();