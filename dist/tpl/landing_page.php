<main class="lp__container">
	<div class="lp__content text--center">
		<h1 class="heading">Site Checklist</h1>
		<form method="GET" action="/">
			<input type="text" name="site" placeholder="Enter site url" class="form__text-input" autofocus/>
		</form>

		<?php if ($error_messages):	?>
		<div class="errors">
			<ul>
			<?php foreach ($error_messages as $msg): ?>
				<li><?=$msg;?></li>
			<?php endforeach; ?>
			</ul>
		</div>
		<?php endif; ?>
	</div>
</main>