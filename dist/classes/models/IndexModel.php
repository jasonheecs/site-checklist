<?php
namespace Models;

class IndexModel {
	public $template;

	public function __construct() {
		$this->template = 'tpl/landing_page.php';
	}
}