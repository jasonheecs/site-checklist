<?php
namespace Models;

class SiteChecklist {
	public $template;
	private $language_errors; //list of grammer and spelling errors
	private $headers; //list of <h1> to <h6> tags
	private $staging_links; //list of staging links
	private $page_title; //page title
	private $meta_data; //meta tags and meta content
	public $has_xml_sitemap; //if there is an xml sitemap

	public function __construct() {
		$this->template = 'tpl/checklist.php';
	}

	public function setLanguageErrors($language_tool_xml_str) {
		$xml = simplexml_load_string($language_tool_xml_str);
		$errors = $xml->error;

		$this->language_errors = array();

		foreach ($errors as $error) {
			$language_error = array();
			$language_error['category'] = $error['category'];
			$language_error['context'] = substr($error['context'], (int)$error['contextoffset'], (int)$error['errorlength']);
			$language_error['replacements'] = explode('#', $error['replacements']);
			$this->language_errors[] = $language_error;
		}

		//group array by error categories
		if ($this->language_errors) {
			$grouped_errors = array();

			foreach ($this->language_errors as $language_error) {
				$grouped_errors[(string) $language_error['category']][] = array('context' => $language_error['context'], 'replacements' => $language_error['replacements']);
			}

			$this->language_errors = $grouped_errors;
		}
	}

	public function getLanguageErrors() {
		return $this->language_errors;
	}

	public function setHeaders($headers) {
		$this->headers = $headers;
	}

	public function getHeaders() {
		return $this->headers;
	}

	public function setStagingLinks($staging_links)	{
		$this->staging_links = $staging_links;
	}

	public function getStagingLinks() {
		return $this->staging_links;
	}

	public function setPageTitle($page_title) {
		$this->page_title = $page_title;
	}

	public function getPageTitle() {
		return $this->page_title;
	}

	public function setMetaData($meta_data) {
		$this->meta_data = $meta_data;
	}

	public function getMetaData() {
		return $this->meta_data;
	}
}